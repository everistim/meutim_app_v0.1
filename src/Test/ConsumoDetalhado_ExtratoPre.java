package Test;

import java.awt.AWTException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Config.Config_ambiente;
import Controller.modConsumoDetalhado;
import Controller.modHomeConsumoInternet;
import Controller.modHomeConsumoVoz;
import Controller.modLogin;
import Controller.modSaldo;
import io.appium.java_client.android.AndroidDriver;

public class ConsumoDetalhado_ExtratoPre {
//	AppiumDriver driver;
	AndroidDriver driver = Config_ambiente.SubirDriver();
	modLogin ml = new modLogin();
	//modSaldo ms = new modSaldo();
	modConsumoDetalhado cd = new modConsumoDetalhado();
	
  @Test
public void Validar_Login() throws IOException, InterruptedException, AWTException {
	  ml.Logar(driver);
	  
  }
  
 
  @Test (dependsOnMethods = "Validar_Login") //(dependsOnGroups = "Parte1")
public void Validar_Consumo_Detalhado() throws NullPointerException, IOException, InterruptedException, AWTException {
	  cd.ConsumoDetalhadoExtrato(driver);
 }
  
  
  @BeforeClass
  public void beforeClass() throws MalformedURLException {

  }

  @AfterClass
  public void afterClass() {
	  Config_ambiente.Sair();
//	  driver.quit();
  }

}
