package Test;

import java.awt.AWTException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Config.Config_ambiente;
import Controller.modHomeConsumoInternet;
import Controller.modHomeConsumoVoz;
import Controller.modLogin;
import Controller.modSaldo;
import io.appium.java_client.android.AndroidDriver;

public class ConsumoVoz_CardInternet {
//	AppiumDriver driver;
	AndroidDriver driver = Config_ambiente.SubirDriver();
	modLogin ml = new modLogin();
	modHomeConsumoInternet mh = new modHomeConsumoInternet();
	modHomeConsumoVoz mv = new modHomeConsumoVoz();
	modSaldo ms = new modSaldo();
	
  @Test
public void Validar_Login() throws IOException, InterruptedException, AWTException {
	  ml.Logar(driver);
	  
  }
  
  @Test (groups = "Parte1", dependsOnMethods = "Validar_Login")
public void Validar_Home_Consumo_Internet() {
	  mh.ConsumoInternet(driver);
 }
 
  @Test (groups = "Parte1" , dependsOnMethods = "Validar_Home_Consumo_Internet")
public void Validar_Home_Consumo_Voz() {
	  mv.ConsumoVoz(driver);
 }
 
  @Test (dependsOnMethods = "Validar_Home_Consumo_Voz") //(dependsOnGroups = "Parte1")
public void Validar_Home_Saldo() {
	  ms.Saldo(driver);
 }
  
  
  @BeforeClass
  public void beforeClass() throws MalformedURLException {
	  
//	File app = new File("C:\\Users\\tpaivacr\\workspace\\MeuTIM_APP_V0.1\\app\\MeuTIM_5116.apk");
//    	
//    	DesiredCapabilities capabilities = new DesiredCapabilities();
//    	capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
//    	capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
//    	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
//    	capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, "true");
//    	capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.accenture.meutim.activities.LoginActivity");
//    	driver = new AppiumDriver <>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
//    	driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
  }

  @AfterClass
  public void afterClass() {
	  Config_ambiente.Sair();
//	  driver.quit();
  }

}
