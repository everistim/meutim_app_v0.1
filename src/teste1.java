import static org.junit.Assert.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class teste1 {
	
	static AndroidDriver driver;
	  static AppiumDriverLocalService appiumService;
	  static String appiumServiceUrl;
	
	@BeforeClass
	  public static void prepareAndroidForAppium() throws MalformedURLException {
		  File appDir = new File("C:\\Users\\tpaivacr\\workspace\\MeuTIM_APP_V0.1\\app");
	      File app = new File(appDir, "MeuTIM_5116.apk");
	      String Appium_Node_Path="C:/Appium/node.exe";
	      String Appium_JS_Path="C:/Appium/node_modules/appium/bin/appium.js";
	     // File myLogFile =  new File("C:/Temp/", "logapp.txt");  
	      String myLogFile = "C:/Temp/logapp.txt";
	      
	     // appiumService = AppiumDriverLocalService.buildService(new AppiumServiceBuilder().withArgument(GeneralServerFlag.LOG_LEVEL, "debug").withLogFile(myLogFile).usingAnyFreePort().usingDriverExecutable(new File(Appium_Node_Path)).withAppiumJS(new File(Appium_JS_Path)));
	      
			appiumService.start();
	        appiumServiceUrl = appiumService.getUrl().toString();
	        System.out.println("Appium Service Address : - "+ appiumServiceUrl);
		
	        DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("deviceName", "LGK220EISGOZRK");
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("app", app);
			capabilities.setCapability("log", "C:/temp/log.txt");
			//capabilities.setCapability("log-level", "info");
			capabilities.setCapability("autoWebview", "true");
			
			//capabilities.setCapability("appPackage", "com.android.calculator2");
			//capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
			driver = new AndroidDriver<>(new URL(appiumServiceUrl), capabilities);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	        
	        
      
        //DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setCapability("device","LGK220EISGOZRK");

        //mandatory capabilities
        //capabilities.setCapability("deviceName","LGK220EISGOZRK");
        //capabilities.setCapability("platformName","Android");
			
        //other caps
		//capabilities.setCapability("app", app.getAbsolutePath());
        //driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        //driver.configuratorSetWaitForIdleTimeout(30);]
        //driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }
	


	@AfterClass
	public static void End() {
		System.out.println("Stop driver");
		driver.quit();
		System.out.println("Stop appium service");
		appiumService.stop();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		
		  String app_package_name = "br.com.timbrasil.meutim:id/";
	        By userId = By.id(app_package_name + "msisdn");
	        By password = By.id(app_package_name + "password");
	        By aceito = By.id(app_package_name + "cb_acceptTermsofUse");
	        By continua = By.id(app_package_name + "btnContinue");
	        By login_Button = By.id(app_package_name + "sign_in_button");
	        By pula = By.id(app_package_name + "txtJump");
	        By recarga = By.id(app_package_name + "refil_button");
	        By webview = By.id(app_package_name + "generic_webview");
	        By lbn_saldo = By.id(app_package_name + "balance_card_title");
	        

	        driver.findElement(userId).sendKeys("21969074094");
	        driver.findElement(password).sendKeys("3545");
	        driver.findElement(login_Button).click();
	        driver.findElement(aceito).click();
	        driver.findElement(continua).click();
	        driver.findElement(pula).click();
	        
	        
	        
	        System.out.println(driver.getContext());
	        
		 
		 
		 //WebElement childElements;
	        //WebElement parentElement = driver.findElement(By.className("android.widget.LinearLayout")); 
	        //android.widget.RelativeLayout
	        
	        
		List<WebElement> childElements = driver.findElements(By.className("android.widget.LinearLayout"));//driver.findElementsByClassName("android.widget.FrameLayout");
		
		
			//WebElement elem =  childElements.get(1);
			
			WebElement elem3 =  childElements.get(3);
			
			//WebElement elem5 =  driver.findElement(By.className(className));
			
			//MobileElement element = (MobileElement) childElements.get(3);
			
			MobileElement element = (MobileElement) childElements.get(3).findElement(By.className("android.widget.Button"));
			
	        //MobileElement lbn_Saldo = (MobileElement) driver.findElement(lbn_saldo);
	        
	        movePara(element);
	        
	       
				
			
			//movePara(btnRecarga);
			
			//btnRecarga.click();
	        scrollToElementByTextContains("Saldo");
	        MobileElement btnRecarga = (MobileElement) driver.findElement(recarga);
	        btnRecarga.click();
			
	        
//			MobileElement m4u = (MobileElement) driver.findElement(By.className("android.webkit.WebView"));
//			Thread.sleep(6000);
//			//System.out.println(m4u.getAttribute("innerHTML"));
//			
//			Set<String> contextNames = driver.getContextHandles();
//			for (String contextName : contextNames) {
//			System.out.println(contextName);
//			if (contextNames.contains("View")){
//			driver.context(contextName);
//			}
//			}
			
			//String pagUrl = driver.getCurrentUrl();
//			String pagHtml = driver.getPageSource();
//			System.out.println("Codigo: " + pagHtml);
		//	System.out.println("URL: " + m4u.getAttribute("innerHTML"));
			
				//element.click();
			
				//List<WebElement> childText = elem.findElements(By.className("android.widget.TextView"));
			
				
			
			
		//elem.findElements(By.ByTagName)
		
		
		//for(int i = 0; i<childText.size(); i++) {	
		//	System.out.println(childElements.get(i).isDisplayed() + "  " + childElements.get(i).getText()); //downloadButtons.get(i).getAttribute("alt") + "  " +
		//} 
		
		
		//Actions actions = new Actions(driver);
		//  actions.moveToElement(elem3).perform();	
		
		
		 
		 
		 
		 
		 
		 
		 
		 
		 
		
	}
		
	public void movePara(MobileElement element){
		int topY = element.getLocation().getY();
	    int bottomY = topY + element.getSize().getHeight();
	    int centerX = element.getLocation().getX() + (element.getSize().getWidth()/2);
	    driver.swipe(centerX, bottomY, centerX, topY, 2000);
	}
	
	   public void scrollToElementByTextContains(String text) {
		    driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
		            ".scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0));");
		}
	

}
