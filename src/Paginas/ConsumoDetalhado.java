package Paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.By;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ConsumoDetalhado {
	 
	AppiumDriver driver;
//    @AndroidFindBy(xpath = "sign_in_button")
//    public WebElement btn_LOGIN;


	@FindBy(how = How.ID, using = "br.com.timbrasil.meutim:id/balance_card_arrow_image")
	public static WebElement btn_CONSUMODETALHADO;
	
	
	public ConsumoDetalhado(AppiumDriver driver)    {      
	    this.driver = driver;             
	    PageFactory.initElements(driver, this);
         
	}
	
	//Metodos da Pagina
	public static void ConsumoDetalhadoIn_Action(){
		  
	      //Log.info("Realizando um click no Botao Consumo detalhado card Saldo");
	      btn_CONSUMODETALHADO.click();
	}
}