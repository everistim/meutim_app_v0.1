package Paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.By;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class Login {
	 
	AppiumDriver driver;
	
//    @AndroidFindBy(xpath = "msisdn")
//    public WebElement input_MSISDN;
//    
//    @AndroidFindBy(xpath = "password")
//    public WebElement input_PASSWORD;
//    
//    @AndroidFindBy(xpath = "sign_in_button")
//    public WebElement btn_LOGIN;
    
	
//	static AndroidDriver driver;
	@FindBy(how = How.ID, using = "msisdn")
	public WebElement input_MSISDN;

	@FindBy(how = How.ID, using = "login_form")
	public WebElement input_PASSWORD;

	@FindBy(how = How.ID, using = "sign_in_button")
	public WebElement btn_LOGIN;
	
	
//	String app_package_name ="br.com.timbrasil.meutim:id/";
//	@AndroidFindBy(id = "orientation")
	
	
//	By input_MSISDN= By.id(app_package_name+"msisdn");
//	By input_PASSWORD= By.id(app_package_name+"login_form");
//	By btn_LOGIN= By.id(app_package_name+"sign_in_button");
	
	public Login(AppiumDriver driver)    {      
	    this.driver = driver;             
	    //This initElements method will create all WebElements
	    //AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,20);
	    PageFactory.initElements(driver, this);
	    //PageFactory.initElements(fac, this);           
	}
	
	//Metodos da Pagina
	public void LogIn_Action(String sUserName, String sPassword){
		
	      input_MSISDN.sendKeys(sUserName);
	      //Log.info("Utilizando MSISDN: " + sUserName);  
	      input_PASSWORD.sendKeys(sPassword);
	      //Log.info("Preenchendo Senha");  
	      //Log.info("Realizando um click no Botao Enviar");
	      btn_LOGIN.click();
	}
	
//	public boolean Login_Error_ForaAr(){
//	      boolean Er = driver.getPageSource().contains("Desculpe, o sistema est�");
//	      return Er;       
//	}
//	
//	public boolean Login_Invalido(){
//	      boolean Er = driver.getPageSource().contains("Usu�rio ou senha inv�lida");
//	      return Er;       
//	}
}