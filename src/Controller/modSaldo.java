package Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.server.handler.FindElement;

import Config.Util;
import io.appium.java_client.android.AndroidDriver;

public class modSaldo {
	
	long pageLoadTime_Seconds;

	public void Saldo(AndroidDriver driver){
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis///////////////////////////
	Date myDateIni = null;
	Util Ut = new Util(driver);
	StopWatch pageLoad = new StopWatch();
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	try{

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
        ///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
        ////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////                
        ///////////////////////////////////////////////Bloco do Tratamento de erros n�o mapeados///////////////////
	    
    	
    	//CARD_INTERNET
    	myDateIni = new Date();
    	pageLoad.reset();
    	pageLoad.start();
    	
    	Ut.scrollToElementByTextContains("Ver outros saldos");
    	
    	// EXIBIR OUTROS SALDOS
    	driver.findElement(By.id(Ut.OUTROS_SALDOS)).click();;
    	
    	//Ut.movePara(element);
    	       	
    	Ut.EsperaElementoZ(Ut.VALOR_SALDO, 20);
    	Ut.scrollToElementByTextContains("SALDO");
    	//Ut.movePara(Util.CARD_SALDO);
    	
    	boolean ResultadoBusca = Ut.getElementsId(Util.CARD_SALDO, "APP MEU TIM - Jornada Consumo Voz Internet", "Home", "Card Saldo", myDateIni);
    	
    	//System.out.println(Ut.getFonte());
    	
//    	String Tela = Ut.ValidaTela();
//    	Ut.ValidaTela();
//    	System.out.println(Tela);
    	
    	
    	pageLoad.stop();
    	 pageLoadTime_Seconds = pageLoad.getTime() / 1000;
    	 	
    	 
    	 if(ResultadoBusca == false){
    	 System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
     	                                       + ";APP MEU TIM - Jornada Consumo Voz Internet;Home;Card Saldo;NA;Sucesso;NA;"
     	                                       		+ pageLoadTime_Seconds);
    	 	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    	 		+ ";APP MEU TIM - Jornada Consumo Voz Internet;Home;Saldo;NA;Sucesso;NA;"
             			+ pageLoadTime_Seconds);
    	 	//Foto Tela
        	Ut.FotoZ("TelaHome");
    	 } else {    		 
    		 System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    		 	+ ";APP MEU TIM - Jornada Consumo Voz Internet;Home;Card Saldo;NA;Erro;Erro ao carregar elemento ou Rede;"
		 			+ pageLoadTime_Seconds);
    		 Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    		 	+ ";APP MEU TIM - Jornada Consumo Voz Internet;Home;Card Saldo;NA;Erro;Erro ao carregar elemento ou Rede;"
    		 		+ pageLoadTime_Seconds);
    		//Foto Tela
    	    	Ut.FotoZ("Erro_TelaHome");
    	 }
    	
    	    	
    }catch (Exception ee) {
    	 ee.printStackTrace();
	        // Foto Tela Login erro
	  
    	 	//StringWriter sw = new StringWriter();
    	 	//ee.printStackTrace(new PrintWriter(sw));
	        //Ut.FotoZ("Erro_Login");
	        //Log.error("Erro no Login: Erro Gen�rico." + ee.getCause());
	        //Log.error(sw.toString());
	        //Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	        //pageLoad.stop();
    		pageLoad.stop();
	        Long pageLoadTime_Seconds = pageLoad.getTime() / 1000;
	       
	        Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	        	+ ";APP MEU TIM - Jornada Consumo Voz Internet;Home;Card Saldo;NA;Erro;Erro no Home: Erro Gen�rico.;"
	        		+ pageLoadTime_Seconds);
	   System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	   					+ ";APP MEU TIM - Jornada Consumo Voz Internet;Home;Card Saldo;NA;Erro;Erro no Home: Erro Gen�rico.;"
	                                       + pageLoadTime_Seconds);
	       
	        //AssertJUnit.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
	    }
    }
}
