package Controller;

import java.awt.AWTException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;

import Config.Util;
import Paginas.Login;
import io.appium.java_client.android.AndroidDriver;

public class modLogin {
	
	public void Logar(AndroidDriver driver) throws IOException, InterruptedException, AWTException{
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis///////////////////////////
	Date myDateIni = null;
	Util Ut = new Util(driver);
	Login LoginPage;
	StopWatch pageLoad = new StopWatch();
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	long pageLoadTime_Seconds;
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis///////////////////////////

    try{

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
        ///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
        ////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////                
        ///////////////////////////////////////////////Bloco do Tratamento de erros n�o mapeados///////////////////
    	myDateIni = new Date();
    	LoginPage = new Login(driver);
    	//Foto Tela
    	Ut.FotoZ("TelaLogin");
	    
    	LoginPage.LogIn_Action("21981762643", "5806");
    	
    	pageLoad.start();
    	
    	String Tela = Ut.ValidaTela();

    	
    	
    	//Validar se elemento com msg de erro � exibida na tela -- Espera at� 5 segundos
    	boolean msgErro = Ut.EsperaElementoZ(Util.ERRO_LOGIN, 5);
    	pageLoad.stop();	    	
    	//System.out.println("Passou");
    	
    	//Validar Erro no Login
    	if (msgErro == true) {
    		System.out.println(driver.findElementById(Util.ERRO_LOGIN).getText());   
    		
    		//Foto Tela
        	Ut.FotoZ("ErroLogin");
    		
        	pageLoad.stop();	
    			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
    		 System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
             + ";APP MEU TIM - Jornada Consumo Voz Internet;Login;NA;NA;Erro;"+ driver.findElementById(Util.ERRO_LOGIN).getText() +";"
             	+ pageLoadTime_Seconds);
    		 
    		Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    			+ ";APP MEU TIM - Jornada Consumo Voz Internet;Login;NA;NA;Erro;"+ driver.findElementById(Util.ERRO_LOGIN).getText() +";"
             		+ pageLoadTime_Seconds);
    		 
    			Assert.assertFalse("Erro Login", msgErro==true);
    	} else {    	
    		
    	//Segue para Tela Home    	
    
    	Ut.EsperaTrocaTela(Tela, 3);
        driver.findElementById(Ut.ACEITO).click();
        //Foto Tela
//    	Ut.FotoZ("TelaConfirma");
        driver.findElementById(Ut.CONTINUA).click();
        //Foto Tela
//    	Ut.FotoZ("TelaContinua");
        driver.findElementById(Ut.PULA).click();
        //Foto Tela
//    	Ut.FotoZ("TelaPula");
        driver.findElementById(Ut.NOTIFICACAO).click();
    	
        pageLoadTime_Seconds = pageLoad.getTime() / 1000;
        System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    	                                       + ";APP MEU TIM - Jornada Consumo Voz Internet;Login;NA;NA;Sucesso;NA;"
    	                                       + pageLoadTime_Seconds);
        	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
        		+ ";APP MEU TIM - Jornada Consumo Voz Internet;Login;NA;NA;Sucesso;NA;"
        			+ pageLoadTime_Seconds);
    	}
    	    	
    }catch (Exception ee) {
    	 ee.printStackTrace();
	        // Foto Tela Login erro
    	 	Ut.FotoZ("Erro_Login");
	//        StringWriter sw = new StringWriter();
	//        ee.printStackTrace(new PrintWriter(sw));
	        //Ut.FotoZ("Erro_Login");
	        //Log.error("Erro no Login: Erro Gen�rico." + ee.getCause());
	        //Log.error(sw.toString());
	        //Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	        //pageLoad.stop();
    	pageLoad.stop();	
        pageLoadTime_Seconds = pageLoad.getTime() / 1000;
      
	   Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	   		+ ";APP MEU TIM - Jornada Consumo Voz Internet;Login;NA;NA;Erro;Erro no Login: Erro Gen�rico.;"
	   			+ pageLoadTime_Seconds);
	   System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	                                       + ";APP MEU TIM - Jornada Consumo Voz Internet;Login;NA;NA;Erro;Erro no Login: Erro Gen�rico.;"
	                                       	+ pageLoadTime_Seconds);
	                                       // Comentar na vers�o final
	       
	        //AssertJUnit.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
	    }
    }
}
