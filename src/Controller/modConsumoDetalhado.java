package Controller;

import java.awt.AWTException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Config.Util;
import Paginas.ConsumoDetalhado;
import io.appium.java_client.android.AndroidDriver;

public class modConsumoDetalhado {
	
	long pageLoadTime_Seconds;

	public void ConsumoDetalhadoExtrato(AndroidDriver driver) throws IOException, InterruptedException, AWTException{
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis///////////////////////////
	Date myDateIni = null;
	Util Ut = new Util(driver);
	StopWatch pageLoad = new StopWatch();
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	boolean ResultadoBusca;
	try{

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
        ///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
        ////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////                
        ///////////////////////////////////////////////Bloco do Tratamento de erros n�o mapeados///////////////////
	    
    	
    	//CARD_INTERNET
    	myDateIni = new Date();
    	pageLoad.reset();
    	pageLoad.start();
    	
    	Ut.scrollToElementByTextContains("SALDO");
    	
    	//AQUI TENTANDO CLICAR DA FORMA QUE VC FEZ
    	//ConsumoDetalhado.ConsumoDetalhadoIn_Action();
    	WebElement e = driver.findElement(By.id("br.com.timbrasil.meutim:id/balance_card_arrow_image"));
    	e.click();
    	
    	WebElement ex = driver.findElement(By.id("br.com.timbrasil.meutim:id/imgDetailedUsageTopArrow"));
    	ex.click();
    	
    	WebElement check = driver.findElement(By.id("br.com.timbrasil.meutim:id/cb_free_of_charge"));
    	check.click();
    	
    	WebElement lig = driver.findElement(By.id("br.com.timbrasil.meutim:id/btnDetailedUsageCalls"));
    	lig.click();
    	
    	WebElement outros = driver.findElement(By.id("br.com.timbrasil.meutim:id/btnDetailedUsageOthers"));
    	outros.click();
    	
    	WebElement internet = driver.findElement(By.id("br.com.timbrasil.meutim:id/btnDetailedUsageInternet"));
    	internet.click();
    	
    	WebElement torpedo = driver.findElement(By.id("br.com.timbrasil.meutim:id/btnDetailedUsageSMS"));
    	torpedo.click();
    	
    	WebElement recarga = driver.findElement(By.id("br.com.timbrasil.meutim:id/btnDetailedUsageCredits"));
    	recarga.click();
    	
    	WebElement filtro = driver.findElement(By.id("br.com.timbrasil.meutim:id/btnHeaderDetailedUsageFilter"));
    	filtro.click();
    	
    	
    	
    	//Ut.scrollToElementByTextContains("SALDO");
    	
    	//Ut.movePara(element);
    	       	
    	//Ut.EsperaElementoZ(Ut.VALOR_SALDO, 20);
    	//Ut.scrollToElementByTextContains("SALDO");
    	//Ut.movePara(Util.CARD_SALDO);
    	
    	// VALIDA SE O ELEMENTO ESTA PRESENTE
    	if (Ut.isElementPresent(Util.CONSUMO_DETALHADO_ID_SEM_INFORMACAO) == true ){
    		ResultadoBusca = Ut.getElementsId(Util.CONSUMO_DETALHADO_ID_SEM_INFORMACAO, "APP MEU TIM - Jornada Consumo Detalhado Extrato", "Consumo Detalhado", "Card Filtro", myDateIni);
    	}else{
    		ResultadoBusca = Ut.getElementsId(Util.CONSUMO_DETALHADO_ID_COM_INFORMACAO, "APP MEU TIM - Jornada Consumo Detalhado Extrato", "Consumo Detalhado", "Card Filtro", myDateIni);
    	}
    	
    	//boolean ResultadoBusca = Ut.getElementsId(Util.CONSUMO_DETALHADO_ID_SEM_INFORMACAO, "APP MEU TIM - Jornada Consumo Detalhado Extrato", "Consumo Detalhado", "Card Filtro", myDateIni);
    	//boolean ResultadoBusca1 = Ut.getElementsId(Util.CONSUMO_DETALHADO_ID_COM_INFORMACAO, "APP MEU TIM - Jornada Consumo Detalhado Extrato", "Consumo Detalhado", "Card Filtro", myDateIni);
    	//System.out.println(Ut.getFonte());
    	
    	//String Tela = Ut.ValidaTela();
//    	Ut.ValidaTela();
//    	System.out.println(Tela);
    	
    	
    	pageLoad.stop();
    	 pageLoadTime_Seconds = pageLoad.getTime() / 1000;
    	 	
    	 
    	 if(ResultadoBusca == false){
    	 System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
     	                                       + ";APP MEU TIM - Jornada Consumo Detalhado;Home;Card Consumo Detalhado;NA;Sucesso;NA;"
     	                                       		+ pageLoadTime_Seconds);
    	 	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    	 		+ ";APP MEU TIM - Jornada Consumo Detalhado;Home;Card Consumo Detalhado;NA;Sucesso;NA;"
             			+ pageLoadTime_Seconds);
    	 	//Foto Tela
        	Ut.FotoZ("TelaConsumoDetalhado");
    	 } else {    		 
    		 System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    		 	+ ";APP MEU TIM - Jornada Consumo Detalhado;Home;Consumo Detalhado;NA;Erro;Erro ao carregar elemento ou Rede;"
		 			+ pageLoadTime_Seconds);
    		 Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
    		 	+ ";APP MEU TIM - Jornada Consumo Detalhado;Home;Consumo Detalhado;NA;Erro;Erro ao carregar elemento ou Rede;"
    		 		+ pageLoadTime_Seconds);
    		//Foto Tela
    	    	Ut.FotoZ("Erro_TelaHome");
    	 }
    	
    	    	
    }catch (Exception ee) {
    	 ee.printStackTrace();
	        // Foto Tela Login erro
	  
    	 	//StringWriter sw = new StringWriter();
    	 	//ee.printStackTrace(new PrintWriter(sw));
	        //Ut.FotoZ("Erro_Login");
	        //Log.error("Erro no Login: Erro Gen�rico." + ee.getCause());
	        //Log.error(sw.toString());
	        //Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	        //pageLoad.stop();
    		pageLoad.stop();
	        Long pageLoadTime_Seconds = pageLoad.getTime() / 1000;
	       
	        Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	        	+ ";APP MEU TIM - Jornada Consumo Detalhado;Home;Consumo Detalhado;NA;Erro;Erro no Home: Erro Gen�rico.;"
	        		+ pageLoadTime_Seconds);
	   System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	   					+ ";APP MEU TIM - Jornada Consumo Detalhado;Home;Consumo Detalhado;NA;Erro;Erro no Home: Erro Gen�rico.;"
	                                       + pageLoadTime_Seconds);
	       
	        //AssertJUnit.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
	    }
    }
}
