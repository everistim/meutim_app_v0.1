package Config;

import java.awt.AWTException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.StartsActivity;


public class Util<V> {
	static String Evid = "C:/SitesTIM_Robo_ContaOnline/Mobile/Evidencias/";
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	
//	static AppiumDriver driver;
	static AndroidDriver driver;
	public Util(AndroidDriver driver){
		Util.driver = driver;
	}
	
	
	public String ValidaTela(){
		String Tela = ((StartsActivity) driver).currentActivity();
		return Tela;
	}
	
	public boolean EsperaTrocaTela(String Tela, int T){
		
		boolean Mudou = false;
		
		if (this.ValidaTela() != Tela){
			return true;
		}
		
	 	  //This loop will rotate for 25 times to check If page Is ready after every 1 second.
		  //You can replace your value with 25 If you wants to Increase or decrease wait time.
		  for (int i=0; i<T; i++){ 
		   try {
		    Thread.sleep(1000);
		    }catch (InterruptedException e) {} 
		   //To check page ready state.
		   if (this.ValidaTela() != Tela){
			   System.out.println(this.ValidaTela() + " " + Tela);
			   Mudou = true;
				break;
		   	}
		  }
		return Mudou;		
	}
	
	
	public boolean isElementPresent(String Elemento) {
	    try {
	      driver.findElementById(Elemento);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }
	
public boolean EsperaElementoZ(String Elemento, int T){
		WebDriverWait wait = new WebDriverWait(driver, 1);
		boolean Achou = this.isElementPresent(Elemento);
		
		if (Achou == true){
			return true;
		}
		
	 	  //This loop will rotate for 25 times to check If page Is ready after every 1 second.
		  //You can replace your value with 25 If you wants to Increase or decrease wait time.
		  for (int i=0; i<T; i++){ 
		   try {
		    Thread.sleep(1000);
		    }catch (InterruptedException e) {} 
		   //To check page ready state.
		   Achou = this.isElementPresent(Elemento);
		   if (Achou == true){
			    Achou = true;
				break;
		   	}
		  }		  
		return Achou;		
	}
	
	
	  public boolean EsperaElemento(String Xp) {
		  boolean resultado = false;
    	   WebDriverWait wait = new WebDriverWait(driver, 2);
		  return resultado = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xp))).isDisplayed();
   }
	  
	  public boolean getElementsXpath(String element,String Jornada, String page, String card, Date myDateIni){			
			//Seleciona um componente de arcordo com o xpath definido e jogo em um objeto do tipo WebElement	
		WebElement e = driver.findElement(By.xpath(element));
				
		//Busca todos os elementos de um determinado tipo, pela className dentro do objeto WebElement carregado anteriormente e joga em uma lista
		List <WebElement> downloadButtons = e.findElements(By.className("android.widget.TextView"));
			
		boolean ErroLoad = false;	
		
		for(int i = 0; i<downloadButtons.size(); i++) {
			    //System.out.println(downloadButtons.get(i).isDisplayed() + "  " + downloadButtons.get(i).getText() + " " );	
			    	//String arquivotxt = (dateFormated + ";" + hourFormated + ";" + page + ";" + card + ";" + downloadButtons.get(i).getText() + ";" + downloadButtons.get(i).isDisplayed() + ";" + "00:00:03");
			
			System.out.println(downloadButtons.get(i).getLocation());
			String Elemento = "";
			 String Motivo = "";
			 String Retorno = "";
			 		 
			if (downloadButtons.get(i).getText().isEmpty() || downloadButtons.get(i).getText().contains("null") || downloadButtons.get(i).getText().contains("Falha ao carregar") || downloadButtons.get(i).getText().contains("Toque para") ){
			    	Motivo = "Componente em branco ou Nulo";
			    	Retorno = "Erro";
			    	Elemento = "NA";
			    	 ErroLoad = true;
			   } else {
				   Elemento =  downloadButtons.get(i).getText();
				   Retorno = "Sucesso";
				   Motivo = "NA";
			   }	
			    System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		   		+ ";" + Jornada + ";" + page  +  ";" + card + ";" + Elemento + ";" +  Retorno + ";" + Motivo + ";"
	   			+ "");
			
			    this.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		   		+ ";" + Jornada + ";" + page  +  ";" + card + ";" + Elemento + ";" +  Retorno + ";" + Motivo + ";"
		   			+ ""); 			   
			   } 
			
		return ErroLoad;
			
	  }	
	  
	  public boolean getElementsId(String element,String Jornada, String page, String card, Date myDateIni){			
			//Seleciona um componente de arcordo com o xpath definido e jogo em um objeto do tipo WebElement	
		WebElement e = driver.findElement(By.id(element));
				
		//Busca todos os elementos de um determinado tipo, pela className dentro do objeto WebElement carregado anteriormente e joga em uma lista
		List <WebElement> downloadButtons = e.findElements(By.className("android.widget.TextView"));
			
		boolean ErroLoad = false;	
		
		for(int i = 0; i<downloadButtons.size(); i++) {
			    //System.out.println(downloadButtons.get(i).isDisplayed() + "  " + downloadButtons.get(i).getText() + " " );	
			    	//String arquivotxt = (dateFormated + ";" + hourFormated + ";" + page + ";" + card + ";" + downloadButtons.get(i).getText() + ";" + downloadButtons.get(i).isDisplayed() + ";" + "00:00:03");
			
			System.out.println(downloadButtons.get(i).getLocation());
			String Elemento = "";
			 String Motivo = "";
			 String Retorno = "";
			 		 
			if (downloadButtons.get(i).getText().isEmpty() || downloadButtons.get(i).getText().contains("null") || downloadButtons.get(i).getText().contains("Falha ao carregar") || downloadButtons.get(i).getText().contains("Toque para") ){
			    	Motivo = "Componente em branco ou Nulo";
			    	Retorno = "Erro";
			    	Elemento = "NA";
			    	 ErroLoad = true;
			   } else {
				   Elemento =  downloadButtons.get(i).getText();
				   Retorno = "Sucesso";
				   Motivo = "NA";
			   }	
			    System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		   		+ ";" + Jornada + ";" + page  +  ";" + card + ";" + Elemento + ";" +  Retorno + ";" + Motivo + ";"
	   			+ "");
			
			    this.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		   		+ ";" + Jornada + ";" + page  +  ";" + card + ";" + Elemento + ";" +  Retorno + ";" + Motivo + ";"
		   			+ ""); 			   
			   } 
			
		return ErroLoad;
			
	  }	
	  
	  public String getFonte(){
		  String source = driver.getPageSource();
		  return source;
	  }
	  
	  
		public void FotoZ(String NomeTela) throws IOException, InterruptedException, AWTException{	
			  SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy_hhmmss");
				String DataArq = dt.format(new Date());
				String Arquivo = Evid + NomeTela + "_" + DataArq + ".png";
				Thread.sleep(1000);
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				File f = new File(Arquivo);//set appropriate path
				FileUtils.copyFile(scrFile, f);
			}
		
		  public String FileWriterLog(String escrita){
	          
		         DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		         Date date = new Date();

		             // String Para data atual
		             String dateFormated = dateFormat.format(date);
		                    
		             //Para instanciar um objeto do tipo File:
		             File arquivo = new File( "C:/SitesTIM_Robo_ContaOnline/Mobile/Dashboard/" + dateFormated +".txt" );
		             try{   
		                    //verifica se o arquivo ou diret�rio existe
		                    boolean existe = arquivo.exists();
		                           
		                    //caso seja um diret�rio, � poss�vel listar seus arquivos e diret�rios
		                    File [] arquivos = arquivo.listFiles();
		                    
		                    //construtor que recebe tamb�m como argumento se o conte�do ser� acrescentado
		                    //ao inv�s de ser substitu�do (append)
		                    FileWriter fwm = new FileWriter( arquivo, true );
		             
		                    //escreve o conte�do no arquivo
		                    fwm.write( escrita + "\r\n");
		                    
		                    //fecha os recursos
		                    fwm.close();
		                    //fw.close();
		             }catch (IOException e) {
		              e.printStackTrace();              
		             }
		             return escrita;
		       }
		
		  public void movePara(String Xp){//MobileElement element
			  	MobileElement element = (MobileElement) driver.findElementByXPath(Xp);
			  	int topY = element.getLocation().getY();
			    int bottomY = topY + element.getSize().getHeight();
			    int centerX = element.getLocation().getX() + (element.getSize().getWidth()/2);
			    driver.swipe(centerX, bottomY, centerX, topY, 2000);
			}
			
			   public void scrollToElementByTextContains(String text) {
				 driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
				            ".scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0));");
				}
		  
		  
	// ---------------------------------------- PAGINA HOME -----------------------------------------------------//
	//public final String CARD_INTERNET = "//android.widget.TextView[@text='INTERNET']/ancestor::android.widget.LinearLayout//android.widget.FrameLayout[@resource-id='br.com.timbrasil.meutim:id/card_view']";
	//public static final String CARD_VOZ = "//android.widget.TextView[@text='MINUTOS']/ancestor::android.widget.LinearLayout//android.widget.FrameLayout[@resource-id='br.com.timbrasil.meutim:id/card_view']//android.widget.RelativeLayout[@resource-id='br.com.timbrasil.meutim:id/rlCardVoiceMain']";
	public final String CARD_INTERNET = "br.com.timbrasil.meutim:id/card_view_data";
	public final String CARD_VOZ = "br.com.timbrasil.meutim:id/card_view_voice";
	
	//public static final String CARD_SALDO = "//android.widget.TextView[@text='SALDO']/ancestor::android.widget.LinearLayout//android.widget.FrameLayout[@resource-id='br.com.timbrasil.meutim:id/balance_card_view']//android.widget.RelativeLayout";
	//public static final String CARD_SALDO = "//android.widget.TextView[@text='SALDO']/ancestor::android.widget.LinearLayout";
	public static final String CARD_SALDO = "br.com.timbrasil.meutim:id/recycleView";
	
	public static final String CONSUMO_DETALHADO_ID_SEM_INFORMACAO = "br.com.timbrasil.meutim:id/lnBalanceHistoryEmptyStateMain";
	public static final String CONSUMO_DETALHADO_ID_COM_INFORMACAO = "br.com.timbrasil.meutim:id/txtCost";
	
	public static final String CARD_ULTIMA_CONTA = "//android.widget.TextView[@text='�LTIMA CONTA']/ancestor::android.widget.LinearLayout//android.widget.LinearLayout[@resource-id='br.com.timbrasil.meutim:id/last_account']";
	public static final String CARD_OFERTAS = "//android.widget.TextView[@text='OFERTAS PARA VOC�']/ancestor::android.widget.LinearLayout[@resource-id='br.com.timbrasil.meutim:id/promotions']";
	
	public static final String NAME_HOME = "br.com.timbrasil.meutim:id/name_shimmer";
	
	public static final String ADDITIONAL_ELEMENT = "br.com.timbrasil.meutim:id/additional_card_view";
	
	public static final String VALOR_SALDO = "br.com.timbrasil.meutim:id/balance_card_recharge_value_basic";
	
	public static final String VALOR_SALDO2 = "br.com.timbrasil.meutim:id/balance_card_reload_item";
	
	public static final String CONSUMO_DETALHADO = "br.com.timbrasil.meutim:id/balance_card_arrow_image";
	
	public static final String OUTROS_SALDOS = "br.com.timbrasil.meutim:id/balance_card_more_detail_textView";

	
	//--------------------------------- MINHA CONTA -----------------------------------------------------------//
	public static final String MINHAS_CONTAS = "br.com.timbrasil.meutim:id/action_bar_root";
	public static final String VER_TODAS_CONTAS = "br.com.timbrasil.meutim:id/see_all_accounts";
	
	public static final String ENTENDA_MAIS = "br.com.timbrasil.meutim:id/linearBlock";
	
	
	//--------------------------------- PERFIL ---------------------------------------------------------------//
	public static final String MEU_PERFIL_PROFILE = "br.com.timbrasil.meutim:id/layout_card_profile";
	public static final String MEU_PERFIL_MEU_PLANO = "//android.widget.TextView[@resource-id='br.com.timbrasil.meutim:id/label_my_plan']";
	
	public static final String MEU_PERFIL_MINUTOS = "br.com.timbrasil.meutim:id/layoutPlanDetailVoice";
	public static final String MEU_PERFIL_INTERNET = "br.com.timbrasil.meutim:id/layoutPlanDetailData";
	public static final String MEU_PERFIL_BENEFICIOS = "br.com.timbrasil.meutim:id/viewMainCardPlanBenefits";
	
	// ---------------------------------- Recarga ------------------------------------------------------------//
	//public static final String RECARGA = "br.com.timbrasil.meutim:id/mtim-menu-accordion";
	
	// ---------------------------------- LOGIN -------------------------------------------------------------//
	public static final String ERRO_LOGIN = "br.com.timbrasil.meutim:id/tvInputAlertMsg";
	public static final String PULA = "br.com.timbrasil.meutim:id/txtJump";
	public static final String ACEITO = "br.com.timbrasil.meutim:id/cb_acceptTermsofUse";
	public static final String CONTINUA = "br.com.timbrasil.meutim:id/btnContinue";
	public static final String NOTIFICACAO = "br.com.timbrasil.meutim:id/btn_cancel";



}
