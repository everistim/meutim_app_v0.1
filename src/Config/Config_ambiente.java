package Config;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class Config_ambiente {
    static //private static ConfigAmb instance=null;
     AndroidDriver driver;
    

    //Construtor
//    public Config_ambiente(){ //WebDriver driver
//    	Config_ambiente.driver = driver;
//    }
                
    public static void Sair(){
    	driver.quit();
    }
    public static AndroidDriver SubirDriver()  {
    	File app = new File("C:/Users/wlustosa/Documents/MeuTim/app-5-8-5804.apk");
    	
    	DesiredCapabilities capabilities = new DesiredCapabilities();
    	capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
    	capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
    	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
    	capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, "true");
    	//capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.accenture.meutim.activities.LoginActivity");
    	
        try {
			driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
    	//WebDriverWait wait = new WebDriverWait(driver, 10);
    	driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		return driver;
    	
    }
}
